package br.com.ebuilders.services.impls;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import br.com.ebuilders.entities.Cliente;
import br.com.ebuilders.repositories.ClienteRepository;
import br.com.ebuilders.services.ClienteService;
/**
 * 
 * Classe resposável por implentar a Interface ClienteService.
 * 
 * @author Ediberto Azevedo
 * @since  15/11/2020
 *
 */
@Service
public class ClienteServiceImpl implements ClienteService {
	
	@Autowired
	private ClienteRepository repository;

	@Override
	public Cliente save(Cliente cliente) {
		return repository.save(cliente);
	}

	@Override
	public void deleteById(Long clienteId) {
		repository.deleteById(clienteId);
	}

	@Override
	public Optional<Cliente> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public Page<Cliente> findAll(Specification<Cliente> spec, Pageable pageable) {
		return repository.findAll(spec, pageable);
	}

	@Override
	public Page<Cliente> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Optional<Cliente> findByCpf(String cpf) {
		return repository.findByCpf(cpf);
	}

}
