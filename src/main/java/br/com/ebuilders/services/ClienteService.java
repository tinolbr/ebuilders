package br.com.ebuilders.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import br.com.ebuilders.entities.Cliente;

/**
 * 
 * Interface resposável por mapear as operações que podem ser realizada para a Entidade Cliente.
 * 
 * @author Ediberto Azevedo
 * @since  15/11/2020
 *
 */
public interface ClienteService {

	Cliente save(Cliente cliente);
	
	void deleteById(Long clienteId);
	
	Optional<Cliente> findById(Long id);
	
	Page<Cliente> findAll(Specification<Cliente> spec, Pageable pageable);
	
	Page<Cliente> findAll(Pageable pageable);
	
	Optional<Cliente> findByCpf(String cpf);
	
}
