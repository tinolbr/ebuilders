package br.com.ebuilders.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.com.ebuilders.entities.Cliente;

/**
 * 
 * Interface resposável por acessa o banco de dados para efetuar as operações do Cliente.
 * 
 * @author Ediberto Azevedo
 * @since  15/11/2020
 *
 */
@Repository
public interface ClienteRepository extends PagingAndSortingRepository<Cliente, Long>, JpaSpecificationExecutor<Cliente>{

	Optional<Cliente> findByCpf(String cpf);

}
