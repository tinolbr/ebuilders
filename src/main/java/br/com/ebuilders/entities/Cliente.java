package br.com.ebuilders.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.hateoas.RepresentationModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * Classe resposável pelo mapeamento do cliente com a tabela do banco de dados.
 * 
 * @author Ediberto Azevedo
 * @since 15/11/2020
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "cliente", uniqueConstraints = {@UniqueConstraint(name = "cpf", columnNames = {"cpf"})})
public class Cliente extends RepresentationModel<Cliente> implements Serializable {

	private static final long serialVersionUID = -205857253526004382L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotEmpty(message = "{nome.notempty}")
	@NotNull(message = "{nome.notempty}")
	@Size(min = 5, max = 100, message = "{nome.size}")
	private String nome;

	@NotEmpty(message = "{cpf.notempty}")
	@NotNull(message = "{cpf.notempty}")
	@CPF(message = "{cpf.invalid}")
	private String cpf;

	@NotNull(message = "{dataNascimento.notempty}")
	private LocalDate dataNascimento;

	/**
	 * 
	 * Método utilizado para calcular a Idade do Cliente.
	 * 
	 * @return Integer
	 */
	@Transient
	public Integer getIdade() {
		return Period.between(this.dataNascimento, LocalDate.now()).getYears();
	}

	/**
	 * 
	 * Método utilizado atualizar os dados (Nome, CPF e DataNascimento) do Cliente.
	 * 
	 * @param cliente
	 */
	public void atualizarDados(Cliente cliente) {
		Optional<Cliente> clienteOptional = Optional.of(cliente);
		clienteOptional.map(Cliente::getNome).map(p -> {
			this.nome = cliente.nome;
			return this;
		});
		clienteOptional.map(Cliente::getCpf).map(p -> {
			this.cpf = cliente.cpf;
			return this;
		});
		clienteOptional.map(Cliente::getDataNascimento).map(p -> {
			this.dataNascimento = cliente.dataNascimento;
			return this;
		});
	}

}