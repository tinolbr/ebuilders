package br.com.ebuilders.controllers.v1;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ebuilders.controllers.filter.ClienteFilter;
import br.com.ebuilders.controllers.filter.base.Filter;
import br.com.ebuilders.controllers.filter.specifications.ClienteSpecification;
import br.com.ebuilders.entities.Cliente;
import br.com.ebuilders.exceptions.ClienteNaoEncontradoException;
import br.com.ebuilders.models.ClienteModel;
import br.com.ebuilders.models.assembler.ClienteAssembler;
import br.com.ebuilders.services.ClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * Classe responsável por expor a interface da API do Cliente
 * 
 * @author Ediberto Azevedo
 * @since 15/11/2020
 *
 */
@Api(tags = {"Cliente API"})
@RestController
@RequestMapping("/api/v1/clientes")
public class ClienteController {

	private static final Logger LOG = LoggerFactory.getLogger(ClienteController.class);

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private ClienteAssembler clienteAssembler;

	@Autowired
	private PagedResourcesAssembler<Cliente> pagedResourcesAssembler;

	/**
	 * 
	 * Método responsável por criar um Cliente.
	 * 
	 * @author Ediberto Azevedo
	 * @since 15/11/2020
	 * 
	 * @param Cliente - Entidade Cliente
	 * 
	 * @return ResponseEntity<EntityModel<ClienteModel>> <code>Response</code> objeto com o HTTP Status
	 * 
	 *         HTTP Status:
	 * 
	 *         201 - Created: É utilizado como resposta de sucesso. 
	 *         400 - Bad Request: Indica que o servidor não pode ou não irá processar a requisição devido a alguma coisa que foi entendida como um erro do cliente. 
	 *         404 - Not Found: Indica que o servidor não conseguiu encontrar o recurso solicitado. 
	 *         409 - Conflict: Indica que a solicitação atual conflitou com o recurso que está no servidor. 
	 *         422 – Unprocessable Entity: Indica que o servidor entende o tipo de onteúdo da entidade da requisição. 
	 *         429 - Too Many Requests: Indica que o usuário enviou muitos pedidos em um determinado período de tempo. 
	 *         500, 502, 503, 504 - Server Errors: Indica que encontrou uma condição inesperada e que o impediu de atender à solicitação.
	 * 
	 */
	@PostMapping
	@ApiOperation(value = "Responsável por criar um Cliente")
	public ResponseEntity<EntityModel<ClienteModel>> incluir(@Valid @RequestBody Cliente cliente) {

		LOG.debug("Inicio - incluir Cliente {}", cliente);

		clienteService.save(cliente);
		
		EntityModel<ClienteModel> model = EntityModel.of(new ClienteModel(cliente));
			model.add(
					linkTo(ClienteController.class)
						.slash(model.getContent().getId())
						.withRel("cliente")
					);
		
		LOG.debug("Fim - Incluir Cliente {}", model);

		return new ResponseEntity<>(model, HttpStatus.OK);
	}

	/**
	 * 
	 * Método responsável por atualizar os dados de um Cliente.
	 * 
	 * @author Ediberto Azevedo
	 * @since 15/11/2020
	 * 
	 * @param id      - Id do Cliente
	 * @param Cliente - Entidade Cliente
	 * 
	 * @return ResponseEntity<EntityModel<ClienteModel>> <code>Response</code> objeto com o HTTP Status
	 * 
	 *         HTTP Status:
	 * 
	 *         201 - Created: É utilizado como resposta de sucesso. 
	 *         400 - Bad Request: Indica que o servidor não pode ou não irá processar a requisição devido a alguma coisa que foi entendida como um erro do cliente. 
	 *         404 - Not Found: Indica que o servidor não conseguiu encontrar o recurso solicitado. 
	 *         409 - Conflict: Indica que a solicitação atual conflitou com o recurso que está no servidor. 
	 *         422 – Unprocessable Entity: Indica que o servidor entende o tipo de onteúdo da entidade da requisição. 
	 *         429 - Too Many Requests: Indica que o usuário enviou muitos pedidos em um determinado período de tempo. 
	 *         500, 502, 503, 504 - Server Errors: Indica que encontrou uma condição inesperada e que o impediu de atender à solicitação.
	 * 
	 */
	@PutMapping("/{id}")
	@ApiOperation(value = "Responsável por atualizar um Cliente Completo")
	public ResponseEntity<EntityModel<ClienteModel>> atualizar(@PathVariable("id") long id, @Valid @RequestBody Cliente cliente) {

		LOG.debug("Inicio - atualizar Cliente {}", cliente);

		Optional<Cliente> clienteToFind = clienteService.findById(id);
		clienteToFind.orElseThrow(() -> new ClienteNaoEncontradoException(id));
		
		clienteToFind.get().atualizarDados(cliente);

		clienteService.save(clienteToFind.get());

		EntityModel<ClienteModel> model = EntityModel.of(new ClienteModel(clienteToFind.get()));	
		model.add(
					linkTo(ClienteController.class)
						.slash(model.getContent().getId())
						.withRel("cliente")
					);

		LOG.debug("Fim - atualizar Cliente {}", model);

		return new ResponseEntity<>(model, HttpStatus.OK);
	}

	/**
	 * 
	 * Método responsável por atualizar parcialmente os dados de um Cliente.
	 * 
	 * @author Ediberto Azevedo
	 * @since 15/11/2020
	 * 
	 * @param id      - Id do Cliente
	 * @param Cliente - Entidade Cliente
	 * 
	 * @return ResponseEntity<EntityModel<Cliente>> <code>Response</code> objeto com o HTTP Status
	 * 
	 *         HTTP Status:
	 * 
	 *         201 - Created: É utilizado como resposta de sucesso. 
	 *         400 - Bad Request: Indica que o servidor não pode ou não irá processar a requisição devido a alguma coisa que foi entendida como um erro do cliente. 
	 *         404 - Not Found: Indica que o servidor não conseguiu encontrar o recurso solicitado. 
	 *         409 - Conflict: Indica que a solicitação atual conflitou com o recurso que está no servidor. 
	 *         422 – Unprocessable Entity: Indica que o servidor entende o tipo de onteúdo da entidade da requisição. 
	 *         429 - Too Many Requests: Indica que o usuário enviou muitos pedidos em um determinado período de tempo. 
	 *         500, 502, 503, 504 - Server Errors: Indica que encontrou uma condição inesperada e que o impediu de atender à solicitação.
	 * 
	 */
	@PatchMapping("/{id}")
	@ApiOperation(value = "Responsável por atualizar um Cliente Parcial")
	public ResponseEntity<EntityModel<ClienteModel>> atualizarParcial(@PathVariable("id") long id, @RequestBody Cliente clienteUpdateOnly) {

		LOG.debug("Inicio - Atualizar Parcialmente Cliente {} {}", id, clienteUpdateOnly);

		Optional<Cliente> clienteToFind = clienteService.findById(id);
		clienteToFind.orElseThrow(() -> new ClienteNaoEncontradoException(id));

		clienteToFind.get().atualizarDados(clienteUpdateOnly);

		clienteService.save(clienteToFind.get());

		EntityModel<ClienteModel> model = EntityModel.of(new ClienteModel(clienteToFind.get()));
		model.add(
				linkTo(ClienteController.class)
					.slash(model.getContent().getId())
					.withRel("cliente")
				);

		LOG.debug("Fim - atualizar Cliente Parcialmente {}", model);

		return new ResponseEntity<>(model, HttpStatus.OK);
	}

	/**
	 * 
	 * Método responsável por deleta um Cliente.
	 * 
	 * @author Ediberto Azevedo
	 * @since 15/11/2020
	 * 
	 * @param id - Id do Cliente
	 * 
	 * @return ResponseEntity<?> <code>Response</code> objeto com o HTTP Status
	 * 
	 *         HTTP Status:
	 * 
	 *         201 - Created: É utilizado como resposta de sucesso. 
	 *         400 - Bad Request: Indica que o servidor não pode ou não irá processar a requisição devido a alguma coisa que foi entendida como um erro do cliente. 
	 *         404 - Not Found: Indica que o servidor não conseguiu encontrar o recurso solicitado. 
	 *         409 - Conflict: Indica que a solicitação atual conflitou com o recurso que está no servidor. 
	 *         422 – Unprocessable Entity: Indica que o servidor entende o tipo de onteúdo da entidade da requisição. 
	 *         429 - Too Many Requests: Indica que o usuário enviou muitos pedidos em um determinado período de tempo. 
	 *         500, 502, 503, 504 - Server Errors: Indica que encontrou uma condição inesperada e que o impediu de atender à solicitação.
	 * 
	 */
	@DeleteMapping("/{id}")
	@ApiOperation(value = "Responsável por deletar um Cliente")
	public ResponseEntity<?> deletar(@PathVariable("id") final long id) {
		return clienteService.findById(id)
				.map(p -> {
						clienteService.deleteById(id);
						return ResponseEntity.noContent().build();
					}).orElse(
							ResponseEntity.notFound().build()
							);
	}

	/**
	 * 
	 * Método responsável por listar todos os clientes.
	 * 
	 * @author Ediberto Azevedo
	 * @since 15/11/2020
	 * 
	 * @param size - Quantidade por pagina.
	 * @param page - Indica a pagina.
	 * 
	 * @return ResponseEntity<?> <code>Response</code> objeto com o HTTP Status
	 * 
	 *         HTTP Status:
	 * 
	 *         201 - Created: É utilizado como resposta de sucesso. 
	 *         400 - Bad Request: Indica que o servidor não pode ou não irá processar a requisição devido a alguma coisa que foi entendida como um erro do cliente. 
	 *         404 - Not Found: Indica que o servidor não conseguiu encontrar o recurso solicitado. 
	 *         409 - Conflict: Indica que a solicitação atual conflitou com o recurso que está no servidor. 
	 *         422 – Unprocessable Entity: Indica que o servidor entende o tipo de onteúdo da entidade da requisição. 
	 *         429 - Too Many Requests: Indica que o usuário enviou muitos pedidos em um determinado período de tempo. 
	 *         500, 502, 503, 504 - Server Errors: Indica que encontrou uma condição inesperada e que o impediu de atender à solicitação.
	 * 
	 */
	@GetMapping
	@ApiOperation(value = "Responsável por pesquisar todos os Clientes com Paginação")
	public ResponseEntity<PagedModel<ClienteModel>> recuperarTodos(Filter filter) {

		LOG.debug("Inicio - recuperarTodos Filter {}", filter);

		PageRequest pageRequest = PageRequest.of(filter.getPage(), filter.getSize(), Sort.Direction.ASC, "nome");
		Page<Cliente> clientes = clienteService.findAll(pageRequest);
		PagedModel<ClienteModel> models = pagedResourcesAssembler.toModel(clientes, clienteAssembler);

		LOG.debug("Fim - recuperarTodos Filter");

		return new ResponseEntity<>(models, HttpStatus.OK);
	}

	/**
	 * 
	 * Método responsável por pesquisar cliente usando Query Strings.
	 * 
	 * @author Ediberto Azevedo
	 * @since 15/11/2020
	 * 
	 * @param nome - Pesquisar pelo nome do cliente.
	 * @param cpf  - Pesquisar pelo CPF do cliente.
	 * @param size - Quantidade por pagina.
	 * @param page - Indica a pagina.
	 * 
	 * @return ResponseEntity<PagedModel<ClienteModel>> <code>Response</code> objeto com o HTTP Status
	 * 
	 *         HTTP Status:
	 * 
	 *         201 - Created: É utilizado como resposta de sucesso. 
	 *         400 - Bad Request: Indica que o servidor não pode ou não irá processar a requisição devido a alguma coisa que foi entendida como um erro do cliente. 
	 *         404 - Not Found: Indica que o servidor não conseguiu encontrar o recurso solicitado. 
	 *         409 - Conflict: Indica que a solicitação atual conflitou com o recurso que está no servidor. 
	 *         422 – Unprocessable Entity: Indica que o servidor entende o tipo de onteúdo da entidade da requisição. 
	 *         429 - Too Many Requests: Indica que o usuário enviou muitos pedidos em um determinado período de tempo. 
	 *         500, 502, 503, 504 - Server Errors: Indica que encontrou uma condição inesperada e que o impediu de atender à solicitação.
	 * 
	 */
	@GetMapping("/query")
	@ApiOperation(value = "Responsável por pesquisar um Cliente usando Filtro")
	public ResponseEntity<PagedModel<ClienteModel>> recuperarPorFiltro(ClienteFilter clienteFilter) {

		LOG.debug("Inicio - recuperarPorFiltro ClienteFilter {} {}", clienteFilter);

		PageRequest pageRequest = PageRequest.of(clienteFilter.getPage(), clienteFilter.getSize(), Sort.Direction.ASC, "nome");
		Specification<Cliente> spec = new ClienteSpecification(clienteFilter);
		Page<Cliente> clientes = clienteService.findAll(spec, pageRequest);
		PagedModel<ClienteModel> models = pagedResourcesAssembler.toModel(clientes, clienteAssembler);

		LOG.debug("Fim - recuperarPorFiltro");

		return new ResponseEntity<>(models, HttpStatus.OK);
	}

	/**
	 * 
	 * Método responsável recuperar um Cliente pelo Id.
	 * 
	 * @author Ediberto Azevedo
	 * @since 15/11/2020
	 * 
	 * @param Id - Id do Cliente
	 * 
	 * @return ResponseEntity<ClienteModel> <code>Response</code> objeto com o HTTP Status
	 * 
	 *         HTTP Status:
	 * 
	 *         201 - Created: É utilizado como resposta de sucesso. 
	 *         400 - Bad Request: Indica que o servidor não pode ou não irá processar a requisição devido a alguma coisa que foi entendida como um erro do cliente. 
	 *         404 - Not Found: Indica que o servidor não conseguiu encontrar o recurso solicitado. 
	 *         409 - Conflict: Indica que a solicitação atual conflitou com o recurso que está no servidor. 
	 *         422 – Unprocessable Entity: Indica que o servidor entende o tipo de onteúdo da entidade da requisição. 
	 *         429 - Too Many Requests: Indica que o usuário enviou muitos pedidos em um determinado período de tempo. 
	 *         500, 502, 503, 504 - Server Errors: Indica que encontrou uma condição inesperada e que o impediu de atender à solicitação.
	 * 
	 */
	@GetMapping("/{id}")
	@ApiOperation(value = "Responsável por recuperar um Cliente pelo seu Id")
	public ResponseEntity<ClienteModel> recuperarPorId(@PathVariable("id") Long id) {
		return clienteService.findById(id)
					.map(clienteAssembler::toModel)
					.map(ResponseEntity::ok)
					.orElse(ResponseEntity.notFound().build());
	}

}