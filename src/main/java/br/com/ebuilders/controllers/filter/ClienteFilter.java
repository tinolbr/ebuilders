package br.com.ebuilders.controllers.filter;

import br.com.ebuilders.controllers.filter.base.Filter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * Classe resposável pelo filtro da pesquisar de Clientes.
 * 
 * @author Ediberto Azevedo
 * @since  15/11/2020
 *
 */
@Data
@Builder
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@AllArgsConstructor
public class ClienteFilter extends Filter {
	
	private String nome;
	private String cpf;

}
