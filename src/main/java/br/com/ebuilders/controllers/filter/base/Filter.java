package br.com.ebuilders.controllers.filter.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * Classe resposável pela paginação dos resultados.
 * 
 * @author Ediberto Azevedo
 * @since  15/11/2020
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Filter {

	private int page = 0;
	private int size = 10;
	
}
