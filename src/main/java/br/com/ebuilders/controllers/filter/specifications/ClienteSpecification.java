package br.com.ebuilders.controllers.filter.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import br.com.ebuilders.controllers.filter.ClienteFilter;
import br.com.ebuilders.entities.Cliente;

/**
 * 
 * Classe resposável por pesquisar Clientes por filtro.
 * 
 * @author Ediberto Azevedo
 * @since  15/11/2020
 *
 */
public class ClienteSpecification implements Specification<Cliente> {
	
	private static final long serialVersionUID = -3712714154813166058L;

	private ClienteFilter filter;

    public ClienteSpecification(ClienteFilter filter) {
        super();
        this.filter = filter;
    }
	
	@Override
	public Predicate toPredicate(Root<Cliente> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		
		Predicate predicate = criteriaBuilder.disjunction();

		if (filter.getNome() != null) {
			predicate.getExpressions()
				.add(criteriaBuilder.like(root.get("nome"), filter.getNome()));
		}
		
		if (filter.getCpf() != null) {
			predicate.getExpressions()
			.add(criteriaBuilder.equal(root.get("cpf"), filter.getCpf()));
		}
		
		return predicate;
	}

}
