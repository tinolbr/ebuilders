package br.com.ebuilders.models.assembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import br.com.ebuilders.controllers.v1.ClienteController;
import br.com.ebuilders.entities.Cliente;
import br.com.ebuilders.models.ClienteModel;
/**
 * 
 * Classe resposável por converter Cliente em ClienteModel.
 * 
 * @author Ediberto Azevedo
 * @since  15/11/2020
 *
 */
@Component
public class ClienteAssembler extends RepresentationModelAssemblerSupport<Cliente, ClienteModel> {

	public ClienteAssembler() {
		super(ClienteController.class, ClienteModel.class);
	}
	
	@Override
	public ClienteModel toModel(Cliente cliente) {
		
		ClienteModel clienteModel = instantiateModel(cliente);
		clienteModel.add(linkTo(
				methodOn(ClienteController.class)
				.recuperarPorId(cliente.getId()))
				.withRel("cliente"));
		
		clienteModel.setId(cliente.getId());
		clienteModel.setNome(cliente.getNome());
		clienteModel.setCpf(cliente.getCpf());
		clienteModel.setDataNascimento(cliente.getDataNascimento());
		clienteModel.setIdade(cliente.getIdade());
		
		return clienteModel;
		
	}

}
