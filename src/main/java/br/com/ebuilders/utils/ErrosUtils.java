package br.com.ebuilders.utils;

import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;

/**
 * 
 * Classe utilitaria para o tratamento de Erros.
 * 
 * @author Ediberto Azevedo
 * @since  15/11/2020
 *
 */
@Component
public class ErrosUtils {
	
	@Autowired
	private MessageSource messages;

	/**
	 * 
	 * Formata a messagem para erros de {@link MethodArgumentNotValidException}.
	 * 
	 * @param status
	 * @param ex
	 * 
	 * @return ResponseEntity<Object>
	 */
	public ResponseEntity<Object> defaultErrorMessage(HttpStatus status, HttpHeaders headers, MethodArgumentNotValidException ex) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", new Date());
		body.put("status", status.value());
		List<String> errors = ex.getBindingResult()
				.getFieldErrors()
				.stream().map(x -> x.getDefaultMessage())
				.collect(Collectors.toList());
		body.put("errors", errors);
		return new ResponseEntity<>(body, headers, status);
	}
	
	/**
	 * 
	 * Formata a messagem para erros de {@link Runtime}.
	 * 
	 * @param status
	 * @param ex
	 * 
	 * @return ResponseEntity<Object>
	 */
	public ResponseEntity<Object> defaultErrorMessage(HttpStatus status, RuntimeException ex) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", new Date());
		body.put("status", status.value());
		List<String> errors = Arrays.asList(new String[] { ex.getMessage() });
		body.put("errors", errors);
		return new ResponseEntity<>(body, status);
	}

	/**
	 * 
	 * Formata a messagem para erros de {@link Exception}.
	 * 
	 * @param status
	 * @param ex
	 * 
	 * @return ResponseEntity<Object>
	 */
	public ResponseEntity<Object> defaultErrorMessage(HttpStatus status, Exception ex) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", new Date());
		body.put("status", status.value());
		List<String> errors = Arrays.asList(new String[] { ex.getMessage() });
		body.put("errors", errors);
		return new ResponseEntity<>(body, status);
	}
	
	/**
	 * 
	 * Formata a mensagem para Chave Duplicada {@link DataIntegrityViolationException}.
	 * 
	 * @param status
	 * @param ex
	 * 
	 * @returnResponseEntity<Object>
	 */
	public ResponseEntity<Object> defaultErrorMessage(HttpStatus status, DataIntegrityViolationException ex) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", new Date());
		body.put("status", status.value());
		String [] key =  new String[] { ((ConstraintViolationException)ex.getCause()).getConstraintName() };
		String message = messages.getMessage("key.duplicate", key, Locale.US);
		List<String> errors = Arrays.asList(new String[] { message });
		body.put("errors", errors);
		return new ResponseEntity<>(body, status);
	}

}
