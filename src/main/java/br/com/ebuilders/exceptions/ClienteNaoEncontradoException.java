package br.com.ebuilders.exceptions;

/**
 * 
 * Classe de exceção resposável por informar  que o cliente não foi encontrado.
 * 
 * @author Ediberto Azevedo
 * @since  15/11/2020
 *
 */
public class ClienteNaoEncontradoException extends RuntimeException {

	/**
	 * Exception ClienteNaoEncontradoException
	 */
	private static final long serialVersionUID = 1L;
	
	public ClienteNaoEncontradoException(Long id) {
		super("Cliente não encontrado, ID " + id + " desconhecido.");
	}
	
}
