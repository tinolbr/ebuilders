package br.com.ebuilders.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * 
 * Classe resposável pelas configurações do Swagger
 * 
 * @author Ediberto Azevedo
 * @since  15/11/2020
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {
	
	@Value("${swagger.release.version}")
	private String releaseVersion;
	
	@Value("${swagger.api.version}")
	private String apiVersion;
	
	
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("br.com.ebuilders.controllers.v1"))
				.paths(PathSelectors.any()).build()
				.apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("Teste Java E-Builders")
				.description("Teste Java E-Builders - Documentação da Interface da API de Cliente.")
				.version(releaseVersion.concat("_").concat(apiVersion))
				.contact(new Contact("Ediberto Azevedo", "", "edibertobsb@hotmail.com"))
				.build();
	}

}
