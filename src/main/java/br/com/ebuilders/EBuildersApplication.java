package br.com.ebuilders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Classe responsável por iniciar a Aplicação
 * 
 * @author Ediberto Azevedo
 * @since  15/11/2020 
 */
@SpringBootApplication
public class EBuildersApplication {

	public static void main(String[] args) {
		SpringApplication.run(EBuildersApplication.class, args);
	}

}
