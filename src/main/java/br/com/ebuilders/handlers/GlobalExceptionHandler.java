package br.com.ebuilders.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.ebuilders.exceptions.ClienteNaoEncontradoException;
import br.com.ebuilders.utils.ErrosUtils;

/**
 * 
 * Classe responsável pelo tratamento de exceções.
 * 
 * @author Ediberto Azevedo
 * @since 15/11/2020
 * 
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private ErrosUtils errorUtils;
	
	/**
	 * 
	 * Tratamento Global para as exceções do bean validation {@link MethodArgumentNotValidException}
	 * 
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		return errorUtils.defaultErrorMessage(status, headers, ex);

	}

	/**
	 * 
	 * Trata a exceção Exception {@link Exception}
	 * 
	 * @param ex
	 * @return ResponseEntity<?>
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handleClienteNaoEncontradoException(Exception ex) {
		return errorUtils.defaultErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR, ex);
	}
	
	/**
	 * 
	 * Trata a exceção {@link ClienteNaoEncontradoException}
	 * 
	 * @param ex
	 * @return ResponseEntity<?>
	 */
	@ExceptionHandler(ClienteNaoEncontradoException.class)
	public ResponseEntity<Object> handleClienteNaoEncontradoException(ClienteNaoEncontradoException ex) {
		return errorUtils.defaultErrorMessage(HttpStatus.NOT_FOUND, ex);
	}
	
	/**
	 * 
	 * Trata a exceção Chave Duplicada {@link DataIntegrityViolationException}
	 * 
	 * @param ex
	 * @return ResponseEntity<?>
	 */
	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<Object> handleSQLIntegrityConstraintViolationException(DataIntegrityViolationException ex) {
		return errorUtils.defaultErrorMessage(HttpStatus.CONFLICT, ex);
	}

}
