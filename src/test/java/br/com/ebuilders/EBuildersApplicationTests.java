package br.com.ebuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.ebuilders.entities.Cliente;
import br.com.ebuilders.services.ClienteService;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles({"test"})
class EBuildersApplicationTests {

	private static final String URL = "/api/v1/clientes";

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private ClienteService service;

	@Test
	void incluirTest() throws JsonProcessingException, Exception {

		mockMvc.perform(
				post(URL).contentType("application/json").content(objectMapper.writeValueAsString(getMockCliente())))
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk());
	}

	@Test
	void atualizarCompletoTest() throws JsonProcessingException, Exception {

		BDDMockito.given(service.findById(Mockito.any(Long.class))).willReturn(Optional.of(getMockCliente()));

		Long id = 1l;
		Cliente cliente = getMockCliente();
		cliente.setId(id);
		cliente.setDataNascimento(LocalDate.of(2011, 12, 26));

		mockMvc.perform(
				put(URL + "/" + id).contentType("application/json").content(objectMapper.writeValueAsString(cliente)))
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk());
	}
	
	@Test
	void atualizarParcialTest() throws JsonProcessingException, Exception {

		BDDMockito.given(service.findById(Mockito.any(Long.class))).willReturn(Optional.of(getMockCliente()));

		Long id = 1l;
		Cliente cliente = getMockCliente();
		cliente.setId(id);
		cliente.setNome("Hianna Joyce do Nascimento Araujo");

		mockMvc.perform(
				patch(URL + "/" + id).contentType("application/json").content(objectMapper.writeValueAsString(cliente)))
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk());
	}
	
	@Test
	void deletarTest() throws JsonProcessingException, Exception {

		BDDMockito.given(service.findById(Mockito.any(Long.class))).willReturn(Optional.of(getMockCliente()));

		Long id = 1l;

		mockMvc.perform(
				delete(URL + "/" + id).contentType("application/json").content(""))
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isNoContent());
	}

	private Cliente getMockCliente() {

		Cliente cliente = new Cliente(null, "Miguel de Araújo Azevedo", "82752128398", LocalDate.of(2010, 12, 26));

		return cliente;
	}

}
